const fs=require("fs");
const path=require("path");


function creatingAndDeletingFiles(){
    try{
    fs.mkdir("./randomFiles",(error)=>{
        if(error){
            console.log(error);
        }else{
            console.log("Directory Created Successfully ....");
        }

    });
    
    for(let index=1;index<10;index++){
        
        let content=`This is file${index}`;
        let fileName=`file.txt${index}.json`;
        let dir="./randomFiles";
        let filePath=path.join(dir,fileName);
        fs.writeFile(filePath,JSON.stringify(content),(error)=>{
            if(error){
                console.log(error);
            }else{
                console.log("File created Successfully...");
        }

        fs.unlink(filePath,(error)=>{
            if(error){
                console.log(error);
            }else{
                console.log("File Deleted Successfully...");
            }
            
        })
    });

    
};
    }catch(error){
        console.log(error);
    }

};

 function removingDirectory(){
    try{
    fs.rm("./randomFiles",{recursive:true},(error)=>{
        if(error){
            console.log(error);
        }else{
            console.log("Directory Deleted Successfully ...");
        }
    });
}catch(error){
    console.log(error);
}
 };

 module.exports={
    creatingAndDeletingFiles,
    removingDirectory
 }

